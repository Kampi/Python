#
# \file Bitmap2Array.py
#
# \brief Convert a monochrome bitmap into a uint8_t array.
#
# Copyright (C) Daniel Kampert, 2018
#	Website: www.kampis-elektroecke.de
#
# GNU GENERAL PUBLIC LICENSE:
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Errors and commissions should be reported to DanielKampert@kampis-elektroecke.de

import os
import sys
import argparse

from PIL import Image

Parser = argparse.ArgumentParser()
Parser.add_argument("-i", "--input", help = "Input path", required = True)
args = vars(Parser.parse_args())

# Open the file as black and white image
BW_Image = Image.open(args["input"]).convert("L")
Width, Height = BW_Image.size

print("[INFO] Use image: {}".format(args["input"]))
print("	-> Widht: {} px".format(Width))
print("	-> Height: {} px".format(Height))

# Load the image
Image = BW_Image.load()

# Write the c file
with open("{}.c".format(os.path.splitext(args["input"])[0]), "w") as File:

	File.write("#include <avr/io.h>")
	File.write("\n")
	File.write("\n")

	File.write("#define PICTURE_{}_WIDTH	{}\n".format(os.path.splitext(args["input"])[0].upper(), Width))
	File.write("#define PICTURE_{}_HEIGHT	{}\n".format(os.path.splitext(args["input"])[0].upper(), Height))
	File.write("\n")

	File.write("const uint8_t Picture{}[] __attribute__((__progmem__)) = {{\n".format(os.path.splitext(args["input"])[0]))

	y = 0
	
	while(y < Height):
		Column = 0

		if((Height - y) > 8):
			dy = 8
		else:
			dy = Height - y

		for x in range(0, Width):
			Byte = 0
			for i in range(0, dy):
				if(Image[x, y + i] > 0):
					Pixel = 1
				else:
					Pixel = 0

				Byte = (Pixel << i) | Byte
			File.write(str(hex(Byte)) + ", ")

		y += dy

		File.write("\n")
	File.write("};")