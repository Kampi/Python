import os
from keras.callbacks import Callback

class EpochCheckpoint(Callback):
	def __init__(self, OutputPath, Every = 5, StartAt = 0):
		super(Callback, self).__init__()

		self.__OutputPath = OutputPath
		self.__Every = Every
		self.__intEpoch = StartAt

	def on_epoch_end(self, epoch, logs = {}):
		if((self.__intEpoch + 1) % self.__Every == 0):
			p = os.path.sep.join([self.__OutputPath, "epoch_{}.hdf5".format(self.__intEpoch + 1)])
			self.model.save(p, overwrite = True)
		self.__intEpoch += 1