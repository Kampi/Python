import os
import json
import numpy as np
import matplotlib.pyplot as plt

from keras.callbacks import BaseLogger

class TrainingMonitor(BaseLogger):
	def __init__(self, FigurePath = None, JSONPath = None, StartAt = 0, Verbose = False):
		super(TrainingMonitor, self).__init__()
		self.__FigurePath = FigurePath
		self.__JSONPath = JSONPath 
		self.__StartAt = StartAt
		self.__Verbose = Verbose

	def on_train_begin(self, logs = dict()):
		self.__H = dict()

		if(self.__JSONPath is not None):
			if(os.path.exists(self.__JSONPath)):
				self.__H = json.loads(open(self.__JSONPath).read())
				if(self.__StartAt > 0):
					for k in self.__H.keys():
						self.__H[k] = self.__H[k][:self.__StartAt]

	def on_epoch_end(self, epoch, logs = dict()):
		for(k, v) in logs.items():
			l = self.__H.get(k, [])
			l.append(v)
			self.__H[k] = l

		if(self.__JSONPath is not None):
			if(self.__Verbose):
				print("[DEBUG] Save JSON at {}...".format(self.__Verbose))
	
			with open(self.__JSONPath + ".json", "w") as File:
				File.write(json.dumps(self.__H))

		if(len(self.__H["loss"]) > 1):
			N = np.arange(self.__StartAt + 1, self.__StartAt + len(self.__H["loss"]) + 1)
			plt.style.use("ggplot")
			plt.figure()
			plt.plot(N, self.__H["loss"], label = "train_loss")
			plt.plot(N, self.__H["val_loss"], label = "val_loss")
			plt.plot(N, self.__H["acc"], label = "train_acc")
			plt.plot(N, self.__H["val_acc"], label = "val_acc")
			plt.title("Training Loss and Accuracy [Epoch {}]".format(len(self.__H["loss"]) + self.__StartAt))
			plt.xlabel("Epoch #")
			plt.ylabel("Loss/Accuracy")
			plt.legend()

			if(self.__Verbose):
				print("[DEBUG] {}".format(logs))

			if(self.__FigurePath is not None):
				if(self.__Verbose):
					print("[DEBUG] Save figure at {}...".format(self.__FigurePath))

				plt.savefig(self.__FigurePath + ".png")

			plt.close()