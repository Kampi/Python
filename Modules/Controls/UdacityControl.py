import socketio
import eventlet
import eventlet.wsgi

from flask import Flask

class UdacityControl(object):
	def __init__(self, Name, Port = 4567):
		self.__sio = socketio.Server()
		self.__app = Flask(Name)
		self.__app = socketio.Middleware(self.__sio, self.__app)
		eventlet.wsgi.server(eventlet.listen(("", Port)), self.__app)
		
	def SendTelemetry(SteeringAngle, Throttle):
		self.__sio.emit("steer", 
						data = { "steering_angle": SteeringAngle.__str__(),
								"throttle": Throttle.__str__()
								},
						skip_sid = True
		)   