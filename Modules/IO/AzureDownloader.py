import os
import cv2
import requests

from requests import exceptions

class AzureDownloader(object):
    def __init__(self, APIKey, OutputPath = "download", GroupSize = 50, URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search", Timeout = 30):
        self.__APIKey = APIKey
        self.__OutputPath = OutputPath
        self.__GroupSize = GroupSize
        self.__URL = URL
        self.__Timeout = Timeout

        self.__EXCEPTIONS = set([IOError, FileNotFoundError, exceptions.RequestException, exceptions.HTTPError, exceptions.ConnectionError, exceptions.Timeout])

    def DownloadDataset(self, SearchKeys):
        for SearchKey in SearchKeys:
            print("[INFO] Search with bing for '{}'...".format(SearchKey))

            # Set the parameter for the request
            self.__Headers = {"Ocp-Apim-Subscription-Key" : self.__APIKey}
            self.__Parameter  = {"q": SearchKey, "offset": 0, "count": self.__GroupSize}
                    
            # Start the search
            response = requests.get(self.__URL, headers = self.__Headers, params = self.__Parameter)
            response.raise_for_status()

            # Get the results
            self.__Results = response.json()

            ImagesFound = self.__Results["totalEstimatedMatches"]
            print("     {} Results for '{}'".format(ImagesFound, SearchKey))

            # Create directory if it does not exist
            if(not(os.path.exists(self.__OutputPath + os.path.sep + SearchKey))):
                os.makedirs(self.__OutputPath + os.path.sep + SearchKey)

            for Offset in range(0, ImagesFound, self.__GroupSize):
                print("     Start request for group {}...".format(Offset + 1))

                # Set the offset for request
                self.__Parameter["offset"] = Offset

                # Start a new request
                search = requests.get(self.__URL, headers = self.__Headers, params = self.__Parameter)
                search.raise_for_status()

                # Get the results
                self.__Results = search.json()
                for Index, Result in enumerate(self.__Results["value"]):
                    print("     Download image {}/{}".format(Index, Offset + self.__GroupSize))

                    # Get the image from URL
                    try:
                        Image = requests.get(Result["contentUrl"], timeout = self.__Timeout)

                        # Get the image file extension
                        Extension = Result["encodingFormat"]

                        # Save the image to disk
                        ImagePath = os.path.sep.join([self.__OutputPath + os.path.sep + SearchKey, "{}.{}".format(str(Index + Offset).zfill(8), Extension)])
                        with open(ImagePath, "wb") as File:
                            File.write(Image.content)

                    except Exception as e:
                        if(type(e) in self.__EXCEPTIONS):
                            print("[INFO] Skipping: {}".format(Result["contentUrl"]))
                            continue