import os

class FileIO(object):
    @staticmethod
    def ReadFiles(Path):
        ImageList = list()
        LabelList = list()
        
        # Get all subdirectories
        FolderList = os.listdir(Path)

        # Loop over each directory
        for File in FolderList:
            if(os.path.isdir(Path + os.path.sep + File)):
                for Image in os.listdir(Path + os.path.sep + File):
                    # Add the image path to the list
                    ImageList.append(Path + os.path.sep + File + os.path.sep + Image)

                    # Add a label for each image and remove the file extension
                    LabelList.append(File.split(".")[0])
            else:
                ImageList.append(Path + os.path.sep + File)
            
                # Add a label for each image and remove the file extension
                LabelList.append(File.split(".")[0])

        return ImageList, LabelList
