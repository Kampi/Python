import h5py
import numpy
from keras.utils import np_utils

class HDF5DatasetGenerator(object):
    def __init__(self, DB, BatchSize, Preprocessors = None, Aug = None, Binarize = True, Classes = 2):
        """
        Create a new dataset generator.
        
        Arguments:
            DB {str} -- Path to hdf5 file with data
            BatchSize {int} -- Batchsize for reading
        
        Keyword Arguments:
            Preprocessors {list} -- List with preprocessors (default: {None})
            Aug {} -- Data aufmentation (default: {None})
            Binarize {bool} -- Flag to binarize the label (default: {True})
            Classes {int} -- Number of classes for binarization (default: {2})
        """

        self.__BatchSize = BatchSize
        self.__Preprocessors = Preprocessors
        self.__Aug = Aug
        self.__Binarize = Binarize
        self.__Classes = Classes
        self.__DB = h5py.File(DB)
        self.__ImageCount = self.__DB["Labels"].shape[0]

    def __del__(self):
        self.Close()

    def GetImageCount(self):
        """
        Get the image count of the generator.
        
        Returns:
            [int] -- Image count
        """

        return self.__ImageCount

    def Generator(self, Passes = numpy.inf):
        """
        Define a Keras data generator.
        
        Keyword Arguments:
            Passes {int} -- Max. number of iterations (default: {numpy.inf})
        """

        Epochs = 0

        while(Epochs < Passes):
            for i in numpy.arange(0, self.__ImageCount, self.__BatchSize):
                Images = self.__DB["Images"][i: i + self.__BatchSize]
                Labels = self.__DB["Labels"][i: i + self.__BatchSize]

                if(self.__Binarize and self.__Classes is not None):
                    Labels = np_utils.to_categorical(Labels, self.__Classes)

                if(self.__Preprocessors is not None):
                    procImages = []

                    for Image in Images:
                        for p in self.__Preprocessors:
                            Image = p.Preprocess(Image)
                        procImages.append(Image)
                    Images = numpy.array(procImages)

                if(self.__Aug is not None):
                    (Images, Labels) = next(self.__Aug.flow(Images, Labels, batch_size = self.__BatchSize))

                yield (Images, Labels)

            Epochs += 1

    def Close(self):
        """
        Close the dataset writer.
        """

        self.__DB.close()