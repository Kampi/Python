import os
import h5py

class HDF5DatasetWriter(object):
    def __init__(self, Dimensions, OutputPath, DataKey = "Images", BufferSize = 1000, DataTypes = ["float", "str"]):
        """
        Create a new dataset writer.
        
        Arguments:
            Dimensions {set} -- Set with dimensions for the hdf5 file
            OutputPath {str} -- Output path
        
        Keyword Arguments:
            DataKey {str} -- Key for data (default: {"Images"})
            BufferSize {int} -- Size of the buffer (default: {1000})
            DataTypes {list} -- Data type for the data (default: {["float", "str"]})
        """

        # Check if the file exist and throw an overwrite warning
        if(os.path.exists(OutputPath)):
            print("[WARNING] Output path exists. File will be overwritten!")
        self.__Database = h5py.File(OutputPath, "w")

        # Create a new database for the data and the labels
        self.__Dataset = self.__Database.create_dataset(DataKey, Dimensions, dtype = DataTypes[0])

        if(DataTypes[1] == "str"):
            DataType = h5py.special_dtype(vlen = str)
        else:
            DataType = DataTypes[1] 

        self.__Labelset = self.__Database.create_dataset("Labels", (Dimensions[0],), dtype = DataType)

        self.__BufferSize = BufferSize
        self.__Buffer = {"Data": [], "Labels": []}
        self.__BufferIndex = 0

        self.__isOpen = True

    def __del__(self):
        self.Close()

    def __setitem__(self, name, data):
        if(name in self.__Database):
            self.__Database[name].resize(data.shape)
            self.__Database[name][:] = data

    def IsOpen(self):
        """
        Check the state of the file.
        
        Returns:
            [bool] -- Flag to determine if the file is open
        """

        return self.__isOpen

    def AddClasses(self, ClassLabels):
        """
        Add the classes to the file.
        
        Arguments:
            ClassLabels {list} -- List with names of the classes
        """

        if(self.__isOpen):
            # Create a new database for the class labels
            self.__ClassLabels = self.__Database.create_dataset("ClassLabels", (len(ClassLabels), ), dtype = h5py.special_dtype(vlen = str))

            # Save the classes in the hdf5 file
            self.__ClassLabels[:] = ClassLabels

    def AddData(self, Data, Label):
        """
        Add data to the file.
        
        Arguments:
            Data {list} -- List with input data
            Label {list} -- List with labels
        """

        if(self.__isOpen):
            # Add the rows and labels to the buffer
            self.__Buffer["Data"].extend(Data)
            self.__Buffer["Labels"].extend(Label)

            # Check to see if the buffer needs to be flushed to disk
            if(len(self.__Buffer["Data"]) >= self.__BufferSize):
                self.Flush()

    def Flush(self):
        """
        Flush the buffer.
        """

        # Set the counter to the current data size
        DataCounter = self.__BufferIndex + len(self.__Buffer["Data"])

        # Store the data from the buffer at the given positions
        self.__Dataset[self.__BufferIndex:DataCounter] = self.__Buffer["Data"]
        self.__Labelset[self.__BufferIndex:DataCounter] = self.__Buffer["Labels"]

        # Set the new buffer index
        self.__BufferIndex = DataCounter

        # Clear the buffer
        self.__Buffer = {"Data": [], "Labels": []}

    def Close(self):
        """
        Close the file.
        """

        # Check if the database is open
        if(not(self.__Database is None)):
            # Flush the buffer if the buffer isn´t empty
            if(len(self.__Buffer["Data"]) > 0):
                self.Flush()

            # Close the database
            self.__Database.close()
            self.__isOpen = False