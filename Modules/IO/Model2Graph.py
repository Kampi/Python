import os
import h5py
import tensorflow as tf
import keras.backend as Backend

from keras.models import load_model
from tensorflow.contrib import lite
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io

class Model2Graph(object):
	@staticmethod
	def Convert(ModelPath, OutputPath = "output"):
		print("[INFO] Converting model '{}' into TensorFlow graph...".format(ModelPath))

		# Get the model name
		ModelName = os.path.splitext(ModelPath)[0].rsplit(os.path.sep, 1)[-1]

		# Load the model from file
		Model = load_model(filepath = ModelPath)

		# Create a TFLite graph
		print("		Convert to TFLite...")
		Converter = lite.TFLiteConverter.from_keras_model_file(ModelPath)
		open(OutputPath + os.path.sep + ModelName + ".tflite" , "wb") .write(Converter.convert())
		print("			Input: {}".format(Model.layers[0].input.name))
		print("			Output: {}".format(Model.layers[-1].output.name))

		# Create a TensorFlow graph
		print("		Convert to TensorFlow frozen graph...")
		Backend.set_learning_phase(0)
		Session = Backend.get_session()

		OutputCount = len(Model.outputs)
		Temp = [None] * OutputCount
		NodeNames = [None] * OutputCount
		for i in range(OutputCount):
			NodeNames[i] = "output_node" + str(i)
			Temp[i] = tf.identity(Model.outputs[i], name = NodeNames[i])

		constant_graph = graph_util.convert_variables_to_constants(Session, Session.graph.as_graph_def(), NodeNames)    
		graph_io.write_graph(constant_graph, OutputPath, ModelName + ".pb", as_text = False)