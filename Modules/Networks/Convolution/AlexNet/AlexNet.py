from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.regularizers import l2
from keras import backend as K

class AlexNet(object):
	@staticmethod
	def Build(Width, Height, Depth, Classes, Reg = 0.0002):
		Model = Sequential()
		InputShape = (Height, Width, Depth)
		ChanDim = -1

		if(K.image_data_format() == "channels_first"):
			InputShape = (Depth, Height, Width)
			ChanDim = 1

		# Block #1: First CONV => RELU => POOL layer set
		Model.add(Conv2D(96, (11, 11), strides = (4, 4), input_shape = InputShape, padding = "same", kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (3, 3), strides = (2, 2)))
		Model.add(Dropout(0.25))

		# Block #2: Second CONV => RELU => POOL layer set
		Model.add(Conv2D(256, (5, 5), padding = "same", kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (3, 3), strides = (2, 2)))
		Model.add(Dropout(0.25))

		# Block #3: CONV => RELU => CONV => RELU => CONV => RELU
		Model.add(Conv2D(384, (3, 3), padding = "same", kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(Conv2D(384, (3, 3), padding = "same", kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(Conv2D(256, (3, 3), padding = "same", kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (3, 3), strides = (2, 2)))
		Model.add(Dropout(0.25))

		# Block #4: First set of FC => RELU layers
		Model.add(Flatten())
		Model.add(Dense(4096, kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization())
		Model.add(Dropout(0.5))

		# Block #5: Second set of FC => RELU layers
		Model.add(Dense(4096, kernel_regularizer = l2(Reg)))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization())
		Model.add(Dropout(0.5))

		# Block #6: Softmax classifier
		Model.add(Dense(Classes, kernel_regularizer = l2(Reg)))
		Model.add(Activation("softmax"))

		return Model
