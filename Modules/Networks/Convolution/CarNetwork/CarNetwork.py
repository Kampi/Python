from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras.layers import Lambda

class CarNetwork(object):
	@staticmethod
	def Build(Width, Height, Depth):
		Model = Sequential()

		Model.add(Lambda(lambda x: x / 127.5 - 1.0, input_shape = (Height, Width, Depth)))
		Model.add(Conv2D(24, (5, 5), activation = "elu", strides = (2, 2)))
		Model.add(Conv2D(36, (5, 5), activation = "elu", strides = (2, 2)))
		Model.add(Conv2D(48, (5, 5), activation = "elu", strides = (2, 2)))
		Model.add(Conv2D(64, (3, 3), activation = "elu"))
		Model.add(Conv2D(64, (3, 3), activation = "elu"))
		Model.add(Dropout(0.5))
		Model.add(Flatten())
		
		Model.add(Dense(100, activation = "elu"))
		Model.add(Dense(50, activation = "elu"))
		Model.add(Dense(10, activation = "elu"))
		
		Model.add(Dense(1))

		return Model
