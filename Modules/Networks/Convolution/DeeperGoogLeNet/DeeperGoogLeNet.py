from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import AveragePooling2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.models import Model
from keras.layers import concatenate
from keras.regularizers import l2
from keras import backend as K

class DeeperGoogLeNet(object):
	@staticmethod
	def ConvModule(x, K, kX, kY, Stride, ChanDim, Padding = "same", Reg = 0.0005, Name = None):
		(convName, bnName, actName) = (None, None, None)

		if(Name is not None):
			convName = Name + "_conv"
			bnName = Name + "_bn"
			actName = Name + "_act"

		# CONV => BN => RELU
		x = Conv2D(K, (kX, kY), strides = Stride, padding = Padding, kernel_regularizer = l2(Reg), name = convName)(x)
		x = BatchNormalization(axis = ChanDim, name = bnName)(x)
		x = Activation("relu", name = actName)(x)

		return x

	@staticmethod
	def InceptionModule(x, num1x1, num3x3Reduce, num3x3, num5x5Reduce, num5x5, num1x1Proj, ChanDim, Stage, Reg = 0.0005):
		First = DeeperGoogLeNet.ConvModule(x, num1x1, 1, 1,	(1, 1), ChanDim, Reg = Reg, Name = Stage + "_first")

		Second = DeeperGoogLeNet.ConvModule(x, num3x3Reduce, 1, 1, (1, 1), ChanDim, Reg = Reg, Name = Stage + "_second1")
		Second = DeeperGoogLeNet.ConvModule(Second, num3x3, 3, 3, (1, 1), ChanDim, Reg = Reg, Name = Stage + "_second2")

		Third = DeeperGoogLeNet.ConvModule(x, num5x5Reduce, 1, 1, (1, 1), ChanDim, Reg = Reg, Name = Stage + "_third1")
		Third = DeeperGoogLeNet.ConvModule(Third, num5x5, 5, 5,	(1, 1), ChanDim, Reg = Reg, Name = Stage + "_third2")

		Fourth = MaxPooling2D((3, 3), strides = (1, 1), padding = "same", name = Stage + "_pool")(x)
		Fourth = DeeperGoogLeNet.ConvModule(Fourth, num1x1Proj, 1, 1, (1, 1), ChanDim, Reg = Reg, Name = Stage + "_fourth")

		x = concatenate([First, Second, Third, Fourth], axis = ChanDim,	name = Stage + "_mixed")

		return x

	@staticmethod
	def Build(Width, Height, Depth, Classes, Reg = 0.0005):
		InputShape = (Height, Width, Depth)
		ChanDim = -1

		if(K.image_data_format() == "channels_first"):
			InputShape = (Depth, Height, Width)
			ChanDim = 1

		# Block #1: POOL => (CONV * 2) => POOL layer set
		Inputs = Input(shape = InputShape)
		x = DeeperGoogLeNet.ConvModule(Inputs, 64, 5, 5, (1, 1), ChanDim, Reg = Reg, Name = "block1")
		x = MaxPooling2D((3, 3), strides = (2, 2), padding = "same", name = "pool1")(x)
		x = DeeperGoogLeNet.ConvModule(x, 64, 1, 1, (1, 1), ChanDim, Reg = Reg, Name = "block2")
		x = DeeperGoogLeNet.ConvModule(x, 192, 3, 3, (1, 1), ChanDim, Reg = Reg, Name = "block3")
		x = MaxPooling2D((3, 3), strides = (2, 2), padding = "same", name = "pool2")(x)

		# Block #2: 2 * Inception modules => POOL
		x = DeeperGoogLeNet.InceptionModule(x, 64, 96, 128, 16, 32, 32, ChanDim, "3a", Reg = Reg)
		x = DeeperGoogLeNet.InceptionModule(x, 128, 128, 192, 32, 96, 64, ChanDim, "3b", Reg = Reg)
		x = MaxPooling2D((3, 3), strides = (2, 2), padding = "same", name = "pool3")(x)

		# Block #3: 5 * Inception modules => POOL
		x = DeeperGoogLeNet.InceptionModule(x, 192, 96, 208, 16, 48, 64, ChanDim, "4a", Reg = Reg)
		x = DeeperGoogLeNet.InceptionModule(x, 160, 112, 224, 24, 64, 64, ChanDim, "4b", Reg = Reg)
		x = DeeperGoogLeNet.InceptionModule(x, 128, 128, 256, 24, 64, 64, ChanDim, "4c", Reg = Reg)
		x = DeeperGoogLeNet.InceptionModule(x, 112, 144, 288, 32, 64, 64, ChanDim, "4d", Reg = Reg)
		x = DeeperGoogLeNet.InceptionModule(x, 256, 160, 320, 32, 128, 128, ChanDim, "4e", Reg = Reg)
		x = MaxPooling2D((3, 3), strides = (2, 2), padding = "same", name = "pool4")(x)

		# Block #3: POOL => DROPOUT
		x = AveragePooling2D((4, 4), name = "pool5")(x)
		x = Dropout(0.4, name = "do")(x)

		# Block #4: Softmax classifier
		x = Flatten(name = "flatten")(x)
		x = Dense(Classes, kernel_regularizer = l2(Reg), name = "labels")(x)
		x = Activation("softmax", name = "softmax")(x)

		return Model(Inputs, x, name = "googlenet")