from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.regularizers import l2
from keras import backend as K

class FruitClassifier(object):
    @staticmethod
    def Build(Width, Height, Depth, Classes, Reg = 0.0002):
        Model = Sequential()

        # First pair if conv2d and pooling layer
        Model.add(Conv2D(128, (5, 5), input_shape = (Height, Width, Depth), padding = "same"))
        Model.add(Activation("relu"))
        Model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))

        # Second pair of conv2d and pooling layer
        Model.add(Conv2D(64, (5, 5), padding = "same"))
        Model.add(Activation("relu"))
        Model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))

        # Third pair of conv2d and pooling layer
        Model.add(Conv2D(32, (2, 2), padding = "same"))
        Model.add(Activation("relu"))
        Model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))

        # Last conv2d layer
        Model.add(Conv2D(16, (5, 5), padding = "same"))
        Model.add(Activation("relu"))

        # First fully connect
        Model.add(Dense(64))

        # Second fully connect
        Model.add(Dense(32))

        # Softmax classifier
        Model.add(Flatten())
        Model.add(Dense(Classes))
        Model.add(Activation("softmax"))

        return Model
