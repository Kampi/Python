from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense

class LeNet(object):
    @staticmethod
    def Build(Width, Height, Depth, Classes):
        Model = Sequential()

        # Block #1: First CONV => RELU => POOL layer set
        Model.add(Conv2D(20, (5, 5), padding = "same", input_shape = (Height, Width, Depth)))
        Model.add(Activation("relu"))
        Model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))

        # Block #2: Second CONV => RELU => POOL layer set
        Model.add(Conv2D(50, (5, 5), padding = "same"))
        Model.add(Activation("relu"))
        Model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))

        # Block #3: First set of FC => RELU layers
        Model.add(Flatten())
        Model.add(Dense(500))
        Model.add(Activation("relu"))

        # Block #4: Softmax classifier
        Model.add(Dense(Classes))
        Model.add(Activation("softmax"))

        return Model
