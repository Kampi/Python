from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import AveragePooling2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.models import Model
from keras.layers import concatenate
from keras import backend as K

class MiniGoogLeNet(object):
	@staticmethod
	def ConvModule(x, K, kX, kY, Stride, ChanDim, Padding = "same"):
		x = Conv2D(K, (kX, kY), strides = Stride, padding = Padding)(x)
		x = BatchNormalization(axis = ChanDim)(x)
		x = Activation("relu")(x)

		return x

	@staticmethod
	def InceptionModule(x, numK1x1, numK3x3, ChanDim):
		conv_1x1 = MiniGoogLeNet.ConvModule(x, numK1x1, 1, 1, (1, 1), ChanDim)
		conv_3x3 = MiniGoogLeNet.ConvModule(x, numK3x3, 3, 3, (1, 1), ChanDim)
		x = concatenate([conv_1x1, conv_3x3], axis = ChanDim)

		return x

	@staticmethod
	def DownsampleModule(x, K, ChanDim):
		conv_3x3 = MiniGoogLeNet.ConvModule(x, K, 3, 3, (2, 2), ChanDim, Padding = "valid")
		pool = MaxPooling2D((3, 3), strides = (2, 2))(x)
		x = concatenate([conv_3x3, pool], axis = ChanDim)

		return x

	@staticmethod
	def Build(Width, Height, Depth, Classes):
		InputShape = (Height, Width, Depth)
		ChanDim = -1

		if(K.image_data_format() == "channels_first"):
			InputShape = (Depth, Height, Width)
			ChanDim = 1

		Inputs = Input(shape = InputShape)
		
		# Block #1: CONV layer set
		x = MiniGoogLeNet.ConvModule(Inputs, 96, 3, 3, (1, 1), ChanDim)

		# Block #2: 2 * Inception modules => Downsample module
		x = MiniGoogLeNet.InceptionModule(x, 32, 32, ChanDim)
		x = MiniGoogLeNet.InceptionModule(x, 32, 48, ChanDim)
		x = MiniGoogLeNet.DownsampleModule(x, 80, ChanDim)

		# Block #3: 4 * Inception modules => Downsample module
		x = MiniGoogLeNet.InceptionModule(x, 112, 48, ChanDim)
		x = MiniGoogLeNet.InceptionModule(x, 96, 64, ChanDim)
		x = MiniGoogLeNet.InceptionModule(x, 80, 80, ChanDim)
		x = MiniGoogLeNet.InceptionModule(x, 48, 96, ChanDim)
		x = MiniGoogLeNet.DownsampleModule(x, 96, ChanDim)

		# Block #4: 2 * Inception modules => POOL => Dropout
		x = MiniGoogLeNet.InceptionModule(x, 176, 160, ChanDim)
		x = MiniGoogLeNet.InceptionModule(x, 176, 160, ChanDim)
		x = AveragePooling2D((7, 7))(x)
		x = Dropout(0.5)(x)

		# Block #5: Softmax classifier
		x = Flatten()(x)
		x = Dense(Classes)(x)
		x = Activation("softmax")(x)

		return Model(Inputs, x, name = "googlenet")