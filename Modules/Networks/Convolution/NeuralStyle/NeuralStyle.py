import os
import cv2
import numpy as np

from keras import backend as K
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from scipy.optimize import fmin_l_bfgs_b

class NeuralStyle(object):
	def __init__(self, Settings):
		# Store the settings
		self.__Settings = Settings

		# Get the dimensions of the input image
		(w, h) = load_img(self.__Settings["InputPath"]).size
		self.__Dims = (h, w)

        # Check if the output path exist
		if(not(os.path.exists(self.__Settings["OutputPath"]))):
			os.makedirs(self.__Settings["OutputPath"])

		# Load the content image and the style images and bring the dimensions to the input image
		self.__Content = self.Preprocess(self.__Settings["InputPath"])
		self.__Style = self.Preprocess(self.__Settings["StylePath"])
		self.__Content = K.variable(self.__Content)
		self.__Style = K.variable(self.__Style)

		# allocate memory of our output image, then combine the
		# content, style, and output into a single tensor so they can
		# be fed through the network
		self.__Output = K.placeholder((1, self.__Dims[0], self.__Dims[1], 3))
		self.__Input = K.concatenate([self.__Content, self.__Style, self.__Output], axis = 0)

		print("[INFO] Loading model...")
		self.__Model = self.__Settings["net"](weights = "imagenet", include_top = False, input_tensor = self.__Input)

		# build a dictionary that maps the *name* of each layer
		# inside the network to the actual layer *output*
		layerMap = {l.name: l.output for l in self.__Model.layers}

		# extract features from the content layer, then extract the
		# activations from the style image (index 0) and the output
		# image (index 2) -- these will serve as our style features
		# and output features from the *content* layer
		contentFeatures = layerMap[self.__Settings["content_layer"]]
		styleFeatures = contentFeatures[0, :, :, :]
		outputFeatures = contentFeatures[2, :, :, :]

		# compute the feature reconstruction loss, weighting it
		# appropriately
		contentLoss = self.featureReconLoss(styleFeatures, outputFeatures)
		contentLoss *= self.__Settings["content_weight"]

		# initialize our style loss along with the value used to
		# weight each style layer (in proportion to the total number
		# of style layers
		styleLoss = K.variable(0.0)
		weight = 1.0 / len(self.__Settings["style_layers"])

		# loop over the style layers
		for layer in self.__Settings["style_layers"]:
			# grab the current style layer and use it to extract the
			# style features and output features from the *style
			# layer*
			styleOutput = layerMap[layer]
			styleFeatures = styleOutput[1, :, :, :]
			outputFeatures = styleOutput[2, :, :, :]

			# compute the style reconstruction loss as we go
			T = self.StyleReconLoss(styleFeatures, outputFeatures)
			styleLoss += (weight * T)

		# finish computing the style loss, compute the total
		# variational loss, and then compute the total loss that
		# combines all three
		styleLoss *= self.__Settings["style_weight"]
		tvLoss = self.__Settings["tv_weight"] * self.tvLoss(self.__Output)
		totalLoss = contentLoss + styleLoss + tvLoss

		# compute the gradients out of the output image with respect
		# to loss
		grads = K.gradients(totalLoss, self.__Output)
		outputs = [totalLoss]
		outputs += grads

		# the implementation of L-BFGS we will be using requires that
		# our loss and gradients be *two separate functions* so here
		# we create a Keras function that can compute both the loss
		# and gradients together and then return each separately
		# using two different class methods
		self.lossAndGrads = K.function([self.__Output], outputs)

	# Load and preprocess an image
	def Preprocess(self, Path):
		Image = load_img(Path, target_size = self.__Dims)
		Image = img_to_array(Image)
		Image = np.expand_dims(Image, axis = 0)
		Image = preprocess_input(Image)

		return Image

	def Deprocess(self, Image):
		# reshape the image, then reverse the zero-centering by
		# *adding* back in the mean values across the ImageNet
		# training set
		Image = Image.reshape((self.__Dims[0], self.__Dims[1], 3))
		Image[:, :, 0] += 103.939
		Image[:, :, 1] += 116.779
		Image[:, :, 2] += 123.680

		# clip any values falling outside the range [0, 255] and
		# convert the image to an unsigned 8-bit integer
		Image = np.clip(Image, 0, 255).astype("uint8")

		return Image

	def gramMat(self, X):
		# the gram matrix is the dot product between the input
		# vectors and their respective transpose
		features = K.permute_dimensions(X, (2, 0, 1))
		features = K.batch_flatten(features)
		features = K.dot(features, K.transpose(features))

		# return the gram matrix
		return features

	def featureReconLoss(self, styleFeatures, outputFeatures):
		# the feature reconstruction loss is the squared error
		# between the style features and output output features
		return K.sum(K.square(outputFeatures - styleFeatures))

	def StyleReconLoss(self, styleFeatures, outputFeatures):
		# compute the style reconstruction loss where A is the gram
		# matrix for the style image and G is the gram matrix for the
		# generated image
		A = self.gramMat(styleFeatures)
		G = self.gramMat(outputFeatures)

		# compute the scaling factor of the style loss, then finish
		# computing the style reconstruction loss
		scale = 1.0 / float((2 * 3 * self.__Dims[0] * self.__Dims[1]) ** 2)
		loss = scale * K.sum(K.square(G - A))

		# return the style reconstruction loss
		return loss

	def tvLoss(self, X):
		# the total variational loss encourages spatial smoothness in
		# the output page -- here we avoid border pixels to avoid
		# artifacts
		(h, w) = self.__Dims
		A = K.square(X[:, :h - 1, :w - 1, :] - X[:, 1:, :w - 1, :])
		B = K.square(X[:, :h - 1, :w - 1, :] - X[:, :h - 1, 1:, :])
		loss = K.sum(K.pow(A + B, 1.25))

		# return the total variational loss
		return loss

	def Transfer(self, maxEvals = 20):
		# generate a random noise image that will serve as a
		# placeholder array, slowly modified as we run L-BFGS to
		# apply style transfer
		X = np.random.uniform(0, 255, (1, self.__Dims[0], self.__Dims[1], 3)) - 128

		# start looping over the desired number of iterations
		for i in range(0, self.__Settings["Iterations"]):
			# run L-BFGS over the pixels in our generated image to
			# minimize the neural style loss
			print("[INFO] Starting iteration {} of {}...".format(i + 1, self.__Settings["Iterations"]))
			(X, loss, _) = fmin_l_bfgs_b(self.Loss, X.flatten(), fprime = self.Grads, maxfun = maxEvals)
			print("[INFO] End of iteration {}, loss: {:.4e}".format(i + 1, loss))

			# Deprocess the generated image and write it to disk
			image = self.Deprocess(X.copy())
			p = os.path.sep.join([self.__Settings["OutputPath"], "iter_{}.png".format(i)])
			cv2.imwrite(p, image)

	def Loss(self, X):
		X = X.reshape((1, self.__Dims[0], self.__Dims[1], 3))

		return self.lossAndGrads([X])[0]

	def Grads(self, X):
		X = X.reshape((1, self.__Dims[0], self.__Dims[1], 3))
		Output = self.lossAndGrads([X])

		return Output[1].flatten().astype("float64")