from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras import backend as K

class SmallerVGGNet(object):
	@staticmethod
	def Build(Width, Height, Depth, Classes):
		Model = Sequential()
		InputShape = (Height, Width, Depth)
		ChanDim = -1

		if(K.image_data_format() == "channels_first"):
			InputShape = (Depth, Height, Width)
			ChanDim = 1

		# Block #1: CONV => RELU => POOL layer set
		Model.add(Conv2D(32, (3, 3), padding = "same", input_shape = InputShape))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (3, 3)))
		Model.add(Dropout(0.25)) 

		# Block #2: 2 * (CONV => RELU) => POOL layer set
		Model.add(Conv2D(64, (3, 3), padding = "same", input_shape = InputShape))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(Conv2D(64, (3, 3), padding = "same", input_shape = InputShape))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (2, 2)))
		Model.add(Dropout(0.25))

		# Block #3: 2 * (CONV => RELU) => POOL layer set
		Model.add(Conv2D(128, (3, 3), padding = "same",	input_shape = InputShape))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(Conv2D(128, (3, 3), padding = "same", input_shape = InputShape))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization(axis = ChanDim))
		Model.add(MaxPooling2D(pool_size = (2, 2)))
		Model.add(Dropout(0.25))

		# Block #4: FC => RELU layer set
		Model.add(Flatten())
		Model.add(Dense(1024))
		Model.add(Activation("relu"))
		Model.add(BatchNormalization())
		Model.add(Dropout(0.5))

		# Block #5: Softmax classifier
		Model.add(Dense(Classes))
		Model.add(Activation("softmax"))

		return Model