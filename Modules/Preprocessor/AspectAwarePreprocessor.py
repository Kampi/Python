import cv2
import imutils

class AspectAwarePreprocessor(object):
	def __init__(self, Width, Height, Inter = cv2.INTER_AREA):
		"""
		Create a new resize preprocessor.

		Arguments:
			Width {int} -- New width of the images
			Height {int} -- New Height of the images
			Inter {int} -- Interpolation algorithm (default: {cv2.INTER_AREA})
		"""

		self.__Width = Width
		self.__Height = Height
		self.__Inter = Inter

	def Preprocess(self, Image):
		"""
		Resize an image with aspect ratio.

		Arguments:
			Image {numpy.ndarray} -- Source image
			
		Returns:
			{numpy.ndarray} -- Resized image
		"""

		(h, w) = Image.shape[:2]
		dW = 0
		dH = 0

		if(w < h):
			Image = imutils.resize(Image, width = self.__Width, inter = self.__Inter)
			dH = int((Image.shape[0] - self.__Height) / 2.0)
		else:
			Image = imutils.resize(Image, height = self.__Height, inter = self.__Inter)
			dW = int((Image.shape[1] - self.__Width) / 2.0)

		(h, w) = Image.shape[:2]
		Image = Image[dH:h - dH, dW:w - dW]

		return cv2.resize(Image, (self.__Width, self.__Height), interpolation = self.__Inter)