import cv2
import numpy as np

class CropPreprocessor(object):
	def __init__(self, Width, Height, Horiz = True, Inter = cv2.INTER_AREA):

		self.__Width = Width
		self.__Height = Height
		self.__Horiz = Horiz
		self.__Inter = Inter

	def Preprocess(self, Image):
		crops = []

		(h, w) = Image.shape[:2]
		coords = [
			[0, 0, self.__Width, self.__Height],
			[w - self.__Width, 0, w, self.__Height],
			[w - self.__Width, h - self.__Height, w, h],
			[0, h - self.__Height, self.__Width, h]]

		dW = int(0.5 * (w - self.__Width))
		dH = int(0.5 * (h - self.__Height))
		coords.append([dW, dH, w - dW, h - dH])

		for (startX, startY, endX, endY) in coords:
			crop = Image[startY:endY, startX:endX]
			crop = cv2.resize(crop, (self.__Width, self.__Height), interpolation = self.__Inter)
			crops.append(crop)

		if(self.__Horiz):
			mirrors = [cv2.flip(c, 1) for c in crops]
			crops.extend(mirrors)

		return np.array(crops)