from keras.preprocessing.image import img_to_array

class ImageToArrayPreprocessor(object):
	def __init__(self, DataFormat = None):
		"""
		Create a new image to array preprocessor.

		Arguments:
			DataFormat {image_data_format} -- Data format of the image
		"""

		self.__DataFormat = DataFormat

	def Preprocess(self, Image):
		"""
		Convert an image to a numpy array

		Arguments:
			Image {PIL image} -- Source image
			
		Returns:
			{numpy.ndarray} -- Image as array
		"""
		
		return img_to_array(Image, data_format = self.__DataFormat)
