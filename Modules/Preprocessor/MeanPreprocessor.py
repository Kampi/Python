import cv2

class MeanPreprocessor(object):
	def __init__(self, rMean, gMean, bMean):
		"""
		Create a new mean preprocessor.

		Arguments:
			rMean {double} -- Mean value for red
			gMean {double} -- Mean value for green
			bMean {double} -- Mean value for blue
		"""
		
		self.__rMean = rMean
		self.__gMean = gMean
		self.__bMean = bMean

	def Preprocess(self, Image):
		"""
		Subtract the mean from an image.

		Arguments:
			Image {numpy.ndarray} -- Source image
			
		Returns:
			{numpy.ndarray} -- Modified image
		"""
		
		(B, G, R) = cv2.split(Image.astype("float32"))
		R -= self.__rMean
		G -= self.__gMean
		B -= self.__bMean

		return cv2.merge([B, G, R])
