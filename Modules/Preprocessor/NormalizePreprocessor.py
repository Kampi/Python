import cv2 as cv

class NormalizePreprocessor(object):
	def __init__(self, Min = 0.0, Max = 255.0):
		"""
		Create a new normalization preprocessor.

		Arguments:
			Min {float} -- Min value for the images
			Max {float} -- Max value for the images
		"""
		
		self.__Min = Min
		self.__Max = Max

	def Preprocess(self, Image):
		"""
		Normalize an image.

		Arguments:
			Image {numpy.ndarray} -- Source image
			
		Returns:
			{numpy.ndarray} -- Normalized image
		"""
		
		cv.normalize(Image, Image, self.__Min, self.__Max, cv.NORM_MINMAX)
		return Image