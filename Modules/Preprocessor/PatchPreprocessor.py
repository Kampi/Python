from sklearn.feature_extraction.image import extract_patches_2d

class PatchPreprocessor(object):
	def __init__(self, Width, Height):
		"""
		Create a new patch preprocessor.

		Arguments:
			Width {int} -- Width of the image patches
			Height {int} -- Height of the image patches
		"""
		self.__Width = Width
		self.__Height = Height

	def Preprocess(self, Image):
		"""
		Create the image patches.

		Arguments:
			Image {numpy.ndarray} -- Source image
			
		Returns:
			{numpy.ndarray} -- Resized image
		"""
		
		return extract_patches_2d(Image, (self.__Height, self.__Width), max_patches = 1)[0]