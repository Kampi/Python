import cv2
import numpy

class RandomBrightnessPreprocessor(object):
	def __init__(self):
		"""
		Create a new random brightness preprocessor
		"""

		pass

	def Preprocess(self, Image):
		"""
		Add random brightness to the images
		
		Returns:
			[numpy.ndarray] -- Preprocessed image
		"""
		
		# Convert the image from RGB to HSV color space
		HSVImage = cv2.cvtColor(Image, cv2.COLOR_RGB2HSV)
		HSVImage[:, :, 2] = HSVImage[:, :, 2] * 1.0 + 0.4 * (numpy.random.rand() - 0.5)

		return cv2.cvtColor(HSVImage, cv2.COLOR_HSV2RGB)