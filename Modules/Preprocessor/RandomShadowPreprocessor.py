import cv2
import numpy

class RandomShadowPreprocessor(object):
	def __init__(self, Width, Height):
		"""
		Create a new random shadow preprocessor

		Arguments:
			Width {int} -- Width of the input images
			Height {int} -- Height of the input images
		"""

		self.__Width = Width
		self.__Height = Height

	def Preprocess(self, Image):
		"""
		Add random shadow to the images

		Arguments:
			Image {numpy.array} -- Source image

		Returns:
			[numpy.ndarray] -- Preprocessed image
		"""

		# Define a random start- and a endpoint for the seperation line
		X_1, Y_1 = self.__Width * numpy.random.rand(), 0
		X_2, Y_2 = self.__Width * numpy.random.rand(), self.__Height
		
		X_M, Y_M = numpy.mgrid[0:self.__Height, 0:self.__Width]

		# Set all pixel under the line to 1 and all other to 0
		PixelMask = numpy.zeros_like(Image[:, :, 1])
		PixelMask[(Y_M - Y_1) * (X_2 - X_1) - (Y_2 - Y_1) * (X_M - X_1) > 0] = 1
		
		# Create a new filter image with the marked pixels
		FilterMask = PixelMask == numpy.random.randint(2)
		
		# Convert the image to the HSL color space
		HLSImage = cv2.cvtColor(Image, cv2.COLOR_RGB2HLS)

		# Apply random shadow on the lightness channel
		HLSImage[:, :, 1][FilterMask] = HLSImage[:, :, 1][FilterMask] * numpy.random.uniform(low = 0.2, high = 0.5)

		return cv2.cvtColor(HLSImage, cv2.COLOR_HLS2RGB)