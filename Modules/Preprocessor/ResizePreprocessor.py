import cv2
 
class ResizePreprocessor(object):
	def __init__(self, Width, Height, Inter = cv2.INTER_AREA):
		"""
		Create a new resize preprocessor.

		Arguments:
			Width {int} -- New width of the images
			Height {int} -- New Height of the images
			Inter {int} -- Interpolation algorithm (default: {cv2.INTER_AREA})
		"""

		self.__Width = Width
		self.__Height = Height
		self.__Inter = Inter

	def Preprocess(self, Image):
		"""
		Resize an image.

		Arguments:
			Image {numpy.ndarray} -- Source image
			
		Returns:
			{numpy.ndarray} -- Resized image
		"""
	
		return cv2.resize(Image, (self.__Width, self.__Height), interpolation = self.__Inter)