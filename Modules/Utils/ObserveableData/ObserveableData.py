class ObserveableData(object):
    def __init__(self, Value = None):
        self.__Callbacks = []
        self.__Value = Value

    def getValue(self):
        return self.__Value

    def setValue(self, Value, NoCallback = False):
        if(not(self.__Value == Value)):
            self.__Value = Value
            
            if(not(NoCallback)):
                for Callback in self.__Callbacks:
                    Callback(self.__Value)

    def setCallback(self, Callback):
        self.__Callbacks.append(Callback)