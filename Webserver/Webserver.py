import atexit
from threading import Thread
from flask import Flask, render_template, request, redirect, Response
from flask_socketio import SocketIO, emit, disconnect

FlaskNamespace = "MyNamespace"

app = Flask(__name__)
socketio = SocketIO(app)

def BackgroundThread():
    print("Start flask thread...")
    while(True):
        socketio.sleep(1)
        socketio.emit("ThreadResponse", {"data" : "Message from Python..."}, namespace = "/" + str(FlaskNamespace))

def ExitHandler():
    BackgroundWorker._stop()

@socketio.on("my_ping", namespace = "/" + str(FlaskNamespace))
def ping_pong():
    emit("my_pong")

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/state", methods = ["POST"])
def get_post_javascript_data():
    jsdata = request.form["state"]
    print(jsdata)
    return jsdata

if(__name__ == "__main__"):
    BackgroundWorker = Thread(target = BackgroundThread)
    BackgroundWorker.start()
    socketio.run(app, debug = True)