$(document).ready(function() 
{
    var socket = io.connect(location.protocol + "//" + document.domain + ":" + location.port + "/MyNamespace");

    document.getElementById("state").onchange = SetLoggingState;
    
    socket.on("ThreadResponse", function(msg) 
    {
        $("#Log").append("<br>" + $("<div/>").text("Received #" + ": " + msg.data).html());
    });

    function SetLoggingState() 
    {
        var e = document.getElementById("state");

        $.post( "/state", 
        {
            "state" : e.options[e.selectedIndex].value
        });

        $("#Log").append("<br>" + $("<div/>").text("State: " + e.options[e.selectedIndex].value).html());
    }

    var ping_pong_times = [];
    var start_time;
    window.setInterval(function() 
    {
        start_time = (new Date).getTime();
        socket.emit("my_ping");
    }, 1000);

    socket.on("my_pong", function() 
    {
        var latency = (new Date).getTime() - start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30);
        var sum = 0;
        for (var i = 0; i < ping_pong_times.length; i++)
        {
            sum += ping_pong_times[i];
        }
        $("#ping-pong").text(Math.round(10 * sum / ping_pong_times.length) / 10);
   });
});